from __future__ import division
from random import Random
from common.structures import Pack,Solution 
from common.greedy import Greedy
from GRASP.localSearch import LocalSearch

class ACO:

	def __init__(self,packs,amountAnts,universe,k,alpha,iterations,Q=1,initialPheromone=1,pheromones=None,evaporationCoefficient=0.6):
		self.amountAnts = amountAnts
		self.universe = universe
		self.packs = list(packs)
		self.k = k
		# self.pheromones = {}
		# if pheromones == None:
			# self.pheromones = [initialPheromone]*len(packs) #Each Pheromone has asociated a pack	
			# for idx,pack in enumerate(self.packs): #Each Pheromone has asociated a pack
				# self.pheromones[idx] = pack.cost/Q

		self.antsSolution = [0]*amountAnts	
		self.bestSolution = []
		self.remainIterations = iterations

		self.evaporationCoefficient = evaporationCoefficient
		self.Q = Q #A constant for the pheromone calculation
		self.greedyFactor = 0.6
		self.alpha = alpha
		self.beta = (1-alpha)

		self.localSearch = LocalSearch(self.universe, set(self.packs), self.k)

		# self.greedyAlgorithm = self.greedySolution
		self.greedyAlgorithm = self.greedyRandomizedSolutionFor

		#Poner tokens antes de Greedy, ya que este las copia
		for aPack in self.packs:			
			aPack.pheromone = initialPheromone
			aPack.setToken(self.alpha*aPack.effectiveCost + self.beta*aPack.pheromone)
			aPack.alpha = self.alpha
			aPack.beta = self.beta

		#First Greedy Iteration Really Randomized and then increase greedyFactor

	def execute(self):
		self.solutionsAllAlong = []
		while self.remainIterations > 0:
			print 'Quedan iteraciones:',self.remainIterations
			for ant in range(self.amountAnts):
				self.antsSolution[ant] = self.localSearch.execute(self.greedyAlgorithm())
				print 'Hormiga: ',ant, 'Solucion: ',self.antsSolution[ant].cost
			self.greedyAlgorithm = self.greedyRandomizedSolutionFor
			self.updateOptimun()
			self.updatePheromones()
			self.remainIterations -= 1
		return (self.bestSolution,self.solutionsAllAlong)

	def updateOptimun(self):
		self.antsSolution.sort(key = lambda aSolution : aSolution.cost) #O(n.log(n))
		localSearchSolution = self.antsSolution[0]
		self.bestAnt = 0
		print 'Mejor de la iteracion: ',localSearchSolution.cost

		try:		
			if self.bestSolution.cost > localSearchSolution.cost:
				self.bestSolution = localSearchSolution

		except:
			self.bestSolution = localSearchSolution

		self.solutionsAllAlong.append(self.bestSolution.cost)

		print 'Mejor Solucion: ',self.bestSolution.cost

	def updatePheromones(self):
		#Actualizar tokens de cada pack para la siguiente ronda
		# for aPack in self.packs:			
		# 	aPack.pheromone = (1-self.evaporationCoefficient)*aPack.pheromone #Evaporacion
		# 	for ant in range(self.amountAnts): 
		# 		aPack.pheromone += (self.Q/self.costOfPack(aPack.id)) if self.hasAntThisPackInItsLastSolution(ant,aPack.id) else 0
		# 	#Winner deposits double
		# 	aPack.pheromone += (self.Q/self.costOfPack(aPack.id)) if self.hasAntThisPackInItsLastSolution(self.bestAnt,aPack.id) else 0

		for aPack in self.packs:			
			aPack.pheromone = (1-self.evaporationCoefficient)*aPack.pheromone #Evaporacion
			if aPack in self.bestSolution:
				aPack.pheromone += (self.Q/sum(aPack.cost for aPack in self.packs))

			if aPack.pheromone <= 12:
				aPack.pheromone = 20

			if aPack.pheromone >= 100:
				aPack.pheromone = 100

			aPack.setToken(self.alpha*aPack.effectiveCost * self.beta*aPack.pheromone)

		lala = list(aPack.token for aPack in self.packs)
		lala.sort()
		print lala


	def greedyRandomizedSolutionFor(self):
		self.seed = Random().random() #Tengo que calcular el seed de vuelta, si no me da lo mismo siempre
		self.greedy = Greedy(universeCardinal=self.universe, allPacks = self.packs, k = self.k, factor=self.greedyFactor, seed=self.seed, sortKey=lambda aPack: self.alpha*aPack.effectiveCost + self.beta*aPack.pheromone)
		solution = self.greedy.executeFromToken()

		return solution

	def greedySolution(self):
		self.seed = Random().random() #Tengo que calcular el seed de vuelta, si no me da lo mismo siempre
		self.greedy = Greedy(universeCardinal=self.universe, allPacks = self.packs, k = self.k, factor=self.greedyFactor, seed=self.seed, sortKey=lambda aPack: self.alpha*aPack.effectiveCost + self.beta*aPack.pheromone)
		solution = self.greedy.execute()

		return solution

	def hasAntThisPackInItsLastSolution(self,ant,idxPack):
		return any(idxPack == aPack.id for aPack in self.antsSolution[ant])

	def costOfPack(self,idxPack):
		return next(aPack for idx,aPack in enumerate(self.packs) if idx==idxPack).cost

	#La utilidad de un pack, es contar de todos los packs
	#Cuantos hay de cada numero
	#
	# def utilityOfPackForAnt(self,idxPack,ant):
	# 	counter = Counter()
	# 	for pack in self.antsSolution[ant]:
	#		


