'''
Created on Aug 4, 2013

@author: pablo

@idea: Archivo con metodos que compartimos para el main
'''

from common.structures import Pack
from common.parserRAIL import parseRAIL
from common.parserSCP import parseSCP
from common.parserSCP2 import parseSCP2

def cap(max, allPacks):
    for aPack in allPacks:
        aPack.set = set(filter(lambda elem: elem <= max, aPack.set))
     
    return set(filter(lambda aPack: len(aPack.set) != 0, allPacks))  

def packSetFromFile(fileName):
    if 'scp' in fileName:
        (bag,universe) = parseSCP2(fileName)
    else:
        (bag,universe) = parseRAIL(fileName)

    return (Pack.generatePackSetFromBag(bag),universe)


#Dejo un ejemplo de como escribir con comas un CSV por las dudas

# def writeOnCSV():
#     import csv

#     lol = [[1,2,3],[4,5,6],[7,8,9]]
#     item_length = len(lol[0])

#     with open('test.csv', 'wb') as test_file:
#       file_writer = csv.writer(test_file)
#       for i in range(item_length):
#         file_writer.writerow([x[i] for x in lol])