from __future__ import division 
from fabric.api import env
import copy
from ACO.main import executeACO
from GRASP.main import executeGrasp, executeGraspProfilingSolutions
import os,itertools

env.warn_only = True

def run_kmin():
    
    outstream = ""
    
    filename = "scp41"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp42"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp43"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp51"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp52"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp53"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp61"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp62"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp63"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 2, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    
    
    fullPath = 'output/grasp/results_kmin__dat'
    outputFile = open(fullPath, "w")
    outputFile.write(outstream)
    outputFile.close()
    
def run_kmed():
    
    outstream = ""
    
    filename = "scp41"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 7, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp42"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 6, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp43"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 5, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp51"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 13, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp52"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 14, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp53"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 13, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp61"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 17, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp62"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 16, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp63"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 18, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 21, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 21, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 21, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 61, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 60, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 59, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 30, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 31, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 31, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 82, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 83, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 81, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    
    
    fullPath = 'output/grasp/results_kmed__dat'
    outputFile = open(fullPath, "w")
    outputFile.write(outstream)
    outputFile.close()
    
def run_kmax():
    
    outstream = ""
    
    filename = "scp41"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 11, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp42"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 9, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp43"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 8, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp51"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 24, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp52"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 26, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp53"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 24, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp61"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 31, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp62"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 29, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scp63"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 34, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 40, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 39, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpa3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 40, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 119, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 118, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpb3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 115, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 58, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 59, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpc3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 59, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd1"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 162, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd2"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 163, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    filename = "scpd3"
    print filename
    (cost, time) =  executeGrasp(filename+".txt", 100, 159, 0)
    outstream += "%s\t%s\t%s\n"%(filename+".txt", cost, time)
    
    
    fullPath = 'output/grasp/results_kmax__dat'
    outputFile = open(fullPath, "w")
    outputFile.write(outstream)
    outputFile.close()

    
    
# run_kmin()
run_kmed()
run_kmax()