'''
Created on Jul 24, 2013

@author: prago
'''

from collections import Counter

from common.structures import Solution

class LocalSearch:

    def __init__(self, universeCardinal, allPacks, k=1):
        self.universeCardinal = universeCardinal
        self.k = k
        self.allPacks = allPacks

    def execute(self, initialSolution):
        
        self.currentSolution = initialSolution
        complementPacks = self.allPacks.difference(self.currentSolution.packs)
        self.complementSolution = Solution(complementPacks)
            
        self.tryToRemove()

        if self.k > 1:
            self.convertSetsToCounters(self.currentSolution.packs)
            self.convertSetsToCounters(self.complementSolution.packs)

        couldImprove = True
        while couldImprove:
            couldImprove = self.tryToSwap()

        if self.k > 1:
            self.convertCounterToSets(self.currentSolution.packs)

        return self.currentSolution


    # First: try to remove superfluous packs: 
    # reduce/sum all Counters to get a notion of total occurrences per element of the universe, in the solution
    # filter elements with count greater than k
    # if some pack has all its elements between the filtered ones, it's expendable
    
    def tryToRemove(self):
        self.convertSetsToCounters(self.currentSolution.packs)
        
        totalOccurrences =  reduce(lambda bagX, bagY: bagX + bagY, map(lambda aPack: aPack.set, self.currentSolution.packs))
        redundantOnesCounter = Counter({elem: count for elem, count in totalOccurrences.iteritems() if count > self.k})
        redundantOnesSet = self.toSet(redundantOnesCounter)

        self.convertCounterToSets(self.currentSolution.packs)
        
        keepOn = True
        while keepOn:
            keepOn = False
            for aPack in self.currentSolution.packs:
                if (aPack.set.issubset(redundantOnesSet)):
                    redundantOnesCounter = redundantOnesCounter - self.toCounter(aPack.set)
                    redundantOnesCounter = Counter({elem: count for elem, count in redundantOnesCounter.iteritems() if count > self.k})
                    if len(redundantOnesCounter) != 0:
                        keepOn = True
                        redundantOnesSet = self.toSet(redundantOnesCounter)
                    self.fromSolutionXToSolutionY(aPack, self.currentSolution, self.complementSolution)
                    break
    
    def tryToSwap(self):
        for currPack in self.currentSolution.packs:
            for compPack in self.complementSolution.packs:
                if self.canSwap(currPack, compPack):
                    return True
        return False
    

    def canSwap(self, currPack, compPack):
        couldSwap = False

        if compPack.cost < currPack.cost:            
            if self.issubset(currPack.set, compPack.set):
                self.swap(currPack, compPack)
                couldSwap = True
            
        return couldSwap


    def combine(self, setOne, setTwo):
        if self.k == 1:
            return setOne.union(setTwo)
        else:
            return setOne + setTwo

    def issubset(self, setOne, setTwo):
        if self.k == 1:
            return setOne.issubset(setTwo)
        else:
            return len(setOne - setTwo) == 0


    def swap(self, packOne, packTwo):
        self.fromSolutionXToSolutionY(packOne, self.currentSolution, self.complementSolution)
        self.fromSolutionXToSolutionY(packTwo, self.complementSolution, self.currentSolution)
        
    def fromSolutionXToSolutionY(self, aPack, solutionX, solutionY):
        solutionX.packs.remove(aPack)
        solutionX.cost -= aPack.cost
        solutionY.packs.add(aPack)
        solutionY.cost += aPack.cost
        
        
    def convertSetsToCounters(self, aSetOfPacks):        
        map(lambda aPack: self.convertToCounter(aPack), aSetOfPacks)
        
    def convertToCounter(self, aPack):        
        aPack.set =  self.toCounter(aPack.set)
    
    def toCounter(self, aSet):
        return Counter({elem: 1 for elem in aSet})
    
    def convertCounterToSets(self, aSetOfPacks):
        map(lambda aPack: self.convertToSet(aPack), aSetOfPacks)
            
    def convertToSet(self, aPack):
        aPack.set = self.toSet(aPack.set)
        
    def toSet(self, aCounter):
        return set(aCounter.iterkeys())