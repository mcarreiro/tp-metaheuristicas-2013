from __future__ import division

class Pack:
    def __init__(self, idx, aSet, cost, token=1, pheromone=0,alpha=1,beta=0):
        self.id = idx
        self.set = aSet.copy()
        self.cost = cost
        self.effectiveCost = cost / len(aSet)
        self.token = token
        self.pheromone = pheromone
        self.alpha=alpha
        self.beta=beta


    def clone(self):
        return Pack(self.id, self.set.copy(), self.cost,self.token,self.pheromone,self.alpha,self.beta)

    def __str__(self):
        return "{ 'id': %s, 'set': %s, 'cost': %s, 'effectiveCost': %s }" % (self.id,self.set,self.cost,self.effectiveCost)

    def __repr__(self):
        return "{ 'id': %s, 'set': %s, 'cost': %s, 'effectiveCost': %s }" % (self.id,self.set,self.cost,self.effectiveCost)
    
    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return (self.id == other.id)

    @staticmethod
    def generatePackSetFromBag(bag):
        packSet = set()
        i = 0
        for item in bag:
            packSet.add(Pack(i,item[0],item[1]))
            i = i + 1
        return packSet

    def setToken(self,token):
        self.token = token


class Solution:

    def __init__(self, packs, cost=None):
        self.packs = packs
        if cost == None:
            self.cost = sum( map( lambda aPack: aPack.cost, packs ) )
        else:
            self.cost = cost

    def __str__(self):
        return "{ 'packs': %s, 'cost': %s }" % (self.packs, self.cost)

    def __repr__(self):
        return "{ 'packs': %s, 'cost': %s }" % (self.packs, self.cost)

    def __iter__(self):
        for item in self.packs:
            yield item
