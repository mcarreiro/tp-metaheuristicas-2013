#/bin/bash

for i in $(ls scpa1_all-iter_k40__dat); do
        echo $i
        echo "  
        set terminal png
        set output '$i.png'
        set xlabel \"Iteraciones\"
        set ylabel \"Costo\"
        set yrange [69725:69735]
        set key below
        plot    \"$i\" using 1:2 with lines title \"costo vs iteraciones\"
        " > gnuplot
        gnuplot gnuplot
done
rm -rf gnuplot

for i in $(ls scp41_all-iter_k11__dat); do
        echo $i
        echo "  
        set terminal png
        set output '$i.png'
        set xlabel \"Iteraciones\"
        set ylabel \"Costo\"
        set yrange [18850:19050]
        set key below
        plot    \"$i\" using 1:2 with lines title \"costo vs iteraciones\"
        " > gnuplot
        gnuplot gnuplot
done
rm -rf gnuplot


for i in $(ls scp42_all-iter__dat); do
        echo $i
        echo "  
        set terminal png
        set output '$i.png'
        set xlabel \"Iteraciones\"
        set ylabel \"Costo\"
        set yrange [515:550]
        set key below
        plot    \"$i\" using 1:2 with lines title \"costo vs iteraciones\"
        " > gnuplot
        gnuplot gnuplot
done
rm -rf gnuplot


for i in $(ls scpa1_all-iter__dat); do
        echo $i
        echo "  
        set terminal png
        set output '$i.png'
        set xlabel \"Iteraciones\"
        set ylabel \"Costo\"
        set yrange [255:280]
        set key below
        plot    \"$i\" using 1:2 with lines title \"costo vs iteraciones\"
        " > gnuplot
        gnuplot gnuplot
done
rm -rf gnuplot