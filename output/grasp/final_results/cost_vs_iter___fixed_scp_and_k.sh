#/bin/bash

for i in $(ls *_dat); do
        echo $i
        echo "  
        set terminal png
        set output '$i.png'
        set xlabel \"Iteraciones\"
        set ylabel \"Costo\"
        set key below
        set style fill solid border -1
        set style histogram cluster gap 1
        set xtics scale 0.01 font \",0.01\"
        plot    \"$i\" using 2: xtic(1) with histogram title \"0%\", \
                \"$i\" using 4: xtic(1) with histogram title \"25%\", \
                \"$i\" using 6: xtic(1) with histogram title \"50%\", \
                \"$i\" using 8: xtic(1) with histogram title \"75%\", \
                \"$i\" using 10: xtic(1) with histogram title \"100%\"
        " > gnuplot
        gnuplot gnuplot
done
rm -rf gnuplot