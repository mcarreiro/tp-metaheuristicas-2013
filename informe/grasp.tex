La metaheurística GRASP es un método iterativo, donde cada iteración consiste de dos fases: construcción de una solución inicial y búsqueda local. Su nombre viene del inglés $Greedy Randomized Adaptive Search Procedure$, lo que se traduciría a Procedimiento de Búsqueda Adaptativo Goloso Aleatorio. Este nombre deriva de que en la fase constructiva, se construye mediante un metodo goloso, una solución inicial factible. Luego se investiga el vecindario de la misma hasta encontrar un mínimo local en la fase de búsqueda local. Este procedimiento se repite en cada una de las sucesivas iteraciones, hasta que la cantidad prefijada de iteraciones es alcanzada. Cada una produce un resultado diferente e independiente de los demás. A lo largo del algortimo se va guardando la que sea la mejor solución hasta el momento, que es la que se devolverá como resultado final. \\
\indent Para que toda la idea tenga sentido es que se requiere del componente de aleatoriedad. La construción golosa que se realiza se hace en forma aleatoria ya que de no ser así cada una de las iteraciones haría lo mismo. De esta forma se logran construir distintas soluciones iniciales que dan lugar a distintos vecindarios para que investigue la búsqueda local, consiguiendo explorar una mayor extensión de vecindarios y encontrar una mayor cantidad de mínimos locales.

\begin{algorithm}[H]
\caption{GRASP($numeroIteraciones$, $semilla$)}
	\begin{algorithmic}
		\FOR{$k = 1...numeroIteraciones$}
			\STATE{$solucion$ $\gets$ GreedyRandomized($semilla$)}
			\STATE{$solucion$ $\gets$ LocalSearch($solucion$)}
			\STATE{$mejorSolucion$ $\gets$ mejor($mejorSolucion$, $solucion$)}
		\ENDFOR
		\RETURN{mejorSolucion}
	\end{algorithmic}
\end{algorithm}

El \textbf{algoritmo 1} ilustra el pseudo-código del procedimiento $GRASP$ y como interactúa con la construcción golosa aleatoria y la búsqueda local.

\begin{algorithm}[H]
\caption{GreedyRandomized($\alpha$, $semilla$)}
	\begin{algorithmic}
		\STATE{calcular los costos iniciales de los elementos}
		\WHILE{$solucion$ no está completa}
			\STATE{construir lista restringida de candidatos ($RCL$)}
			\STATE{$e$ $\gets$ seleccionarAlAzarDeListaRCL($RCL$, $\alpha$)}
			\STATE{agregar $e$ a $solucion$}
			\STATE{recalcular los costos de los elementos}
		\ENDWHILE
		\RETURN{solucion}
	\end{algorithmic}
\end{algorithm}

En el \textbf{algoritmo 2} se muestran en pseudo-código las ideas generales que conforman la construcción de una solución mediante el método goloso aleatorio. Se observa que se toman dos parámetros, una semilla, que es la que inicializa la secuencia de números pseudo-aleatorios y un $\alpha$ que es el factor de aleatoriedad. \\
\indent Cada elemento candidato a ser añadido a la solución parcial que está siendo generada tiene asociado un costo. Este costo representa cuanto se incrementaría el costo de la solución parcial si se agregara dicho elemento. Dado que esto es lo que queremos minimizar es aquí que econtramos el aspecto goloso, el método intentaría obtener el elemento de menor costo en cada interación. \\
\indent Sin embargo, debemos tener en cuenta el aspecto aleatorio, no es el mejor elemento el que es elegido. Lo que se hace es construir una lista restringida de candidatos: $RCL$ del inglés \textit{Restricted Candidate List}. En esta lista ordenamos los elementos ascendentemente por su costo, pero no tomamos todos, si no los que sean suficientemente buenos. Este criterio se calcula a partir del parámetro $\alpha$. Lo que se hace es, dados el mayor y menor costo entre los elementos disponibles se calcula su diferencia $\Delta$. Luego se determina el $mayorCostoPermitido$ como el menor costo más $\Delta$ por el factor $\alpha$. Finalmente la lista $RCL$ estará confomada por los elementos de costo menor a $mayorCostoPermitido$. Una vez construida la $RCL$ se elige un elemento al azar de ella y se añade a la solución parcial. Esto se muestra en el \textbf{algoritmo 3} 

\begin{algorithm}[H]
\caption{seleccionarAlAzarDeListaRCL($\alpha$, $semilla$)}
	\begin{algorithmic}
		\STATE{$c_{min}$ $\gets$ min\{ costo($e$) $|$ e $\in$ $C$ \} }
		\STATE{$c_{max}$ $\gets$ max\{ costo($e$) $|$ e $\in$ $C$ \} }
		\STATE{$RCL$ $\gets$ \{ $e$ $\in$ $C$ $|$ costo($e$) $\leq$ $c_{min} + \alpha (c_{max} - c_{min})$ \} } 		
		\RETURN{elegir al azar de $RCL$}
	\end{algorithmic}
\end{algorithm}

Notemos que $\alpha$ es un factor, toma valores del 0 al 1, y de alguna manera representa en que porcentage se permite que los elementos sean "peores" que el mejor disponible, para entrar a la $RCL$. A partir de esto se puede ver que asignar un factor $\alpha$ igual a 1 dará lugar a una construcción completamente aleatoria donde el aspecto goloso y la noción de "mejor" son dejadas de lado. Por el contrario un $\alpha$ de 0 conllevará una construcción completamente golosa sin aleatoriedad alguna. Cabe destacar que en la algoritmo desarrollado, por detalles implementativos, si el factor fuera de 0 continuara existiendo un componente aleatorio en los casos que mas de un elemento empate en ser el "mejor" y tener el costo más bajo. \\
\indent Como los costos de cada elemento pueden depender de que haya hasta el momento en la solución parcial, en cada iteración, después de añadir el elemento seleccionado los costos efectivos de cada uno de los elementos disponibles se reevalúan, aquí encontramos el aspecto adaptativo. \\ 
\indent Las soluciones generadas por la construcción aleatoria golosa en la mayoría de los casos no serán óptimas. Es por eso que se procede a la segunda fase, la de búsqueda local, donde se intentará mejorar la solución inicial construida. Este algoritmo utiliza la noción de soluciones vecinas y vecindarios. Esto será propio de cada problema particular. El procedimiento es iterativo y lo que hace es buscar en el vecindario de la soluión actual una solución vecina que sea mejor. En caso de encontrarla reemplaza la solución actual, lo cual da lugar también a un nuevo vecindario actual. Así en la siguiente iteración será este nuevo vecindario el que se evalúe. Notemos que como se requiere que la solución mejor sea estrictamente mejor, nunca entraremos en ciclos en la búsqueda de forma que esta termina. Esto sucederá cuando no sea posible encontrar ninguna solución mejor en el vecindario actual. Podemos ver el pseudo-código de este procedimiento en el \textbf{algoritmo 4}. 

\begin{algorithm}[H]
\caption{LocalSearch($solucionInicial$)}
	\begin{algorithmic}
		\STATE{$solucionActual$ $\gets$ $solucionInicial$}
		\WHILE{$solucionActual$ no sea un óptimo local}
			\STATE{buscar $otraSolucion$ en Vecindario($solucionActual$) tal que costo($otraSolucion$) $<$ costo($solucionActual$)}
			\STATE{}
			\STATE{$solucionActual$ $\gets$ $otraSolucion$}
		\ENDWHILE
		\RETURN{$solucionActual$}
	\end{algorithmic}
\end{algorithm}

La búsqueda local tiene una efectividad que varía de acuerdo a distintos aspectos como la estructura del vecindario, la técnica de búsqueda en el vecindario, la velocidad con la que se evalúen las funciones de costos de las soluciones y la solución inicial. Si bién en el caso general una buena solución inicial es recomendable para llegar a mejores resultados, en el contexto de $GRASP$, es importante que las soluciones iniciales sean variadas para poder explorar mayor cantidad de vecindarios y mínimos locales. Por eso es importante el componente aleatorio. \\
\indent La búsqueda en el vecindario se puede realizar utilizando la técnica de $mayor-mejora$ o $primer-mejora$. En el caso de $mayor-mejora$ todos los vecinos son evaluados mediante la función de costo y se reemplaza la solución actual por el vecino que signifique la mayor mejora, como su nombre lo indica. En el caso de $primer-mejora$, en cuanto se encuentre un vecino cuya función de costo mejore respecto de la solución actual se realizará el reemplazo, el resto del vecindario quedará sin explorar. Al implementarlas, al contrario de lo que podíamos haber supuesto, notamos que ambas técnicas llevaban a resultados similares y sin embargo la de $primer-mejora$ era considerablemente mas rápida, sobre todo para problemas grandes donde nos encontrábamos con tamaños de vecindarios muy extensos. 

\subsection*{Implementación para SCkP}

A continuación se exponen algunos detalles implementativos de la metaheurística $GRASP$ para el problema SCkP que nos permitieron obtener una mejor performance. \\
\indent Recordemos que contamos con un universo $U={1,....,m}$, un k y una familia de conjuntos $F={S_1,....,S_n}$, donde cada $S_i$ es un conjunto de elementos de este universocon un costo asociado $c(S_i)$, y lo que queremos es encontrar un subconjunto de $F$, que llamaremos $G$, tal que por cada elemento $e$ del universo $U$ existen al menos k elementos en $G$ que contengan a $e$. Sujeto a que se minimice la sumatoria de los costos de los $S_i$ que integren $G$.\\


\subsubsection*{Algortimo constructivo goloso}

\indent Para la construcción golosa aleatoria la parte mas dependiente del problema es la función de costos utilizada en la evaluación y reevaluación de los $S_i$ que conformarán la $RCL$. No perdamos de vista que los conjuntos que ya forman parte de la solución parcial dejan de ser candidatos a formar la $RCL$. \\  
\indent Cada $S_i$ tiene un costo propio asociado como parte del $input$ del problema al que llamamos $c(S_i)$. En un primer enfoque uno podría pensar en considerar la función de costo $f_{costo}$ de cada $S_i$ como este costo $c$. Sin embargo, sin pensarlo demasiado notamos que esta sería una decisión apresurada y que no representa adecuadamente la idea de "mejor" asociada al problema. Es importante, además de este costo $c$, tener en cuenta cuantos elementos aporta el $S_i$ en cuestión a la solución. Veamos que podríamos tener $S_1, S_2 \in F$ tales que $c(S_1)=c(S_2)$ pero $|S_1| > |S_2|$, entonces evidentemente nos convendría elegir $S_1$. Así podríamos mejorar la función de costo y tomar la relación entre el cardinal de $S_i$ y su costo $c$: $f_{costo}(S)=\frac{c(S)}{|S|}$. \\
\indent Esta función parecía contemplar adecuadamente la naturaleza del problema, pero al implementarla y empezar a hacer pruebas nos encontramos con un inconveniente. La idea de cardinal del conjunto $S$ y cuantos elementos aportaba a la solución variaba en las sucesivas iteraciones a medida que la solución parcial se iba haciendo mas grande. Notemos que un conjunto con un gran cardinal y bajo costo pordría parecer un candidato ideal pero si todos sus elementos ya están cubiertos en la solución parcial por otros conjuntos agregados anteriormente es inútil, pues no aporta nada. \\
\indent Al notar esto modificamos el algortimo para que haga lo siguiente: después de agregar un nuevo conjunto a la solución parcial se realizará un checkeo en cada uno de los conjuntos que todavía no forman parte de la solución. Para cada uno se evaluará cada elemento, si resulta que este elemento ya está cubierto en la solución parcial se procederá a eliminarlo del conjunto y si al evaluar todos los elementos resultara que el conjunto quedó vacío este dejará de estar disponible para conformar la solución. De esta forma no solo conseguimos una función de costo mucho mas precisa si no que a fines implementativos el algoritmo es mas eficiente pues cada vez revisa menos conjuntos y menos cantidad de elementos por conjunto. Al implementarlo y probarlo efectivamente encontramos soluciones de mejor calidad y mayor eficiencia en tiempo. \\
\indent El útimo cambio que realizamos fue al pensar el problema en términos de SCkP en lugar de SCP. Notemos que si tomamos la solución como la unitoria de los $S_i$ tales que todos los elementos se cubren k veces y se intenta minimizar la sumatoria de los costos, tendremos para el caso k=1 un conjunto pero para k$>$1 esa unitoria será un multiconjunto. De esta forma, notamos que podemos ajustar todavía más la función de costos de manera que no solo se encargue de eliminar elementos que ya están cubiertos en la solución parcial, de los conjuntos que no forman parte de ella. También en caso de que no estén cubiertos, se evalúe en forma pesada el caso de que estén parcialmente cubiertos y cuan cubiertos estén. Para explicar esta situación tomemos el siguiente ejemplo, supongamos que tenemos una instancia del problema con k=8 y nos encontramos en una iteración para la cual la solución parcial es un multiconjunto que contiene el 1 y el 2, dos veces cada uno y el 3 y el 4, seis veces cada uno, tendría una forma similar a: $\{1,1,2,2,3,3,3,3,3,3,4,4,4,4,4,4\}$. Ahora, contamos con dos conjuntos que no están en la solución parcial $S_1=\{1,2\}$ y $S_2=\{3,4\}$. Si bién ninguno de los elementos 1, 2, 3, y 4 están completamente cubiertos en la solución parcial intuitivamente notamos que el 3 y el 4 están $"mas"$ cubiertos. Así nos convendría tomar $S_1$ ya que el 1 y el 2 están $"menos"$ cubiertos. 
Mas precisamente la función de costos toma la forma: \\

$f_{costo}(S)=\frac{c(S)}{\sum\limits_{e \in S} k - \#(e, solucionParcial)}$

donde $\#(e, solucionParcial)$ denota la cantidad de apariciones del elemento $e$ en el multiconjunto conformado por la solución parcial. 

\subsubsection*{Algortimo de búsqueda local}

Aquí es importante definir la idea de solución vecina y vecindario para el problema particular de SCkP. Tomamos como soluciones posibles para el algoritmo solo soluciones factibles del problema. Definiremos que una solución es vecina a otra si difiere en un conjunto $S_i$. Es decir, serán vecinas las soluciones que tengan un conjunto menos, uno mas y que intercambien uno que no está en la solución con uno que si. Como esto es una búsqueda local estricta y no otra técnica como por ejemplo un $Tabu \ search$ siempre buscamos mejorar la solución actual con lo cual no tiene sentido hablar de soluciones que tengan un conjunto mas. El caso de quitar conjuntos tiene sentido para cubrir las situaciones en que la construcción golosa haya agregado cosas "de más". \\
\indent En nuestra implementación intentamos primero quitar conjuntos y a partir de ahí iteramos tratando de realizar intercambios. Como se mencionó anteriormente optamos por elegir el primer vecino del vecindario que mejore la solución actual. Cuando no se puedan realizar mas intercambios se da por finalizado. Se hicieron pruebas con intercambios de dos conjuntos por dos conjuntos, pero para muy raros casos se encontraban mejores soluciones mejores que con intercambios de a uno y los tiempos de ejecución del algoritmo empeoraban mucho. Por esto decidimos que otro tipo de intercambios no valían la pena. \\
\indent Otra cuestión implementativa a tener en cuenta es como se realizan los intercambios de soluciones. Como construir una solución nueva a cada iteración nada mas que para que difiera en un único conjunto con la actual sería poco eficiente tomamos otro enfoque. A lo largo de las iteraciones mantenemos dos colecciones. una con los conjuntos de la solucion actual y otra que representa su complemento, todos los conjuntos que estaban en la entrada, pero no estan en la solución actual. A partir de esto en cada iteración realizamos una exploración combinatoria donde para cada conjunto en la solución revisamos cada conjunto en el complemento. Si el conjunto en el complemento es subconjunto del conjunto en la solución quiere decir que la solución resultante de intercambiarlos seguiría siendo factible, pues todos los elementos seguirían estando cubiertos. Si además el costo del conjunto a agregar es menor que el costo del conjunto a sacar el intercambio resultaría en una solución mejor y efectivamente se realiza. 