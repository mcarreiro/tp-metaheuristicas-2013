import os,itertools

def main():
	ks = ['1','4','7']
	scps = ['scp4', 'scp5', 'scp6', 'scpa', 'scpb', 'scpc', 'scpd', 'scpe']

	merged = ""

	result = dict()

	for k in ks:
		result[k] = dict()
		for scp in scps:
			result[k][scp] = dict()

	for dirpath,dirnames,filenames in os.walk('.'):
		if dirpath == ".":
			for filename in filenames:
				if filename.startswith("var"):
					print filename+"...."

					file_k = filename.split("__k")[1][0]
					file_iterations = filename.split("_iterations")[1][0]

					thefile = open(filename, 'r')
					for line in thefile.readlines():				

						row = line.split('\t')

						scp_type = row[0]
						cost = row[1]
						time = row[2]
						factor = row[3]

						if file_iterations not in result[file_k][scp_type]:
							result[file_k][scp_type][file_iterations] = dict()

						result[file_k][scp_type][file_iterations][factor] = (cost, time)


	print "------------------------------------------------------------------"
	print "--------------------DONE------READING-----------------------------"
	print "------------------------------------------------------------------"

# headers:
# iterations cost_0 time_0 cost_25 time_25 cost_50 time_50 cost_75 time_75 cost_1 time_1  

	for k,scp_keys_dict in result.iteritems():
		for scp, iterations_keys_dict in scp_keys_dict.iteritems():

			print "writing: %s_k%s"%(scp,k)

			outputstream = ""

			iterations_list = iterations_keys_dict.keys()
			iterations_list.sort()
			for iterations in iterations_list:
				factor_keys_dict = iterations_keys_dict[iterations]

				line = ""
				line += str(iterations)

				factors_list = factor_keys_dict.keys()
				factors_list.sort()
				for factor in factors_list:
					cost_time = factor_keys_dict[factor]
					line += "\t"+str(cost_time[0])+"\t"+str(cost_time[1])

				line += "\n"
				print line
				outputstream += line

			if outputstream != "":
				fileout_name = scp+"_k"+k+"_dat"
				fileout = open(fileout_name, 'w')
				fileout.write(outputstream)
				fileout.close()
			else:
				print "not available data for this parameter combination"

	print "------------------------------------------------------------------"
	print "-----------------------FINISHED-----------------------------------"
	print "------------------------------------------------------------------" 

main()