
from structures import Pack
import os.path,sys

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))


def parse(filename):
	data = open(filename, "r")

	n,m = 0,0
	costs = []
	bag = []

	tmp_set = None
	set_size = 0
	set_cost = 0

	try:		

		line = data.readline().strip()
		nm = line.split(" ")
		universo = int(nm[0])
		cantidadDeConjuntos = int(nm[1])

		#Aplanamos la matriz y la posicion i indica el costo del conjunto i
		while len(costs) < cantidadDeConjuntos:
			line = data.readline().strip()
			values = line.split(" ")
			
			for cost in values:
				if cost == '\n':
					break
				costs.append(int(cost))


		#Creamos los conjuntos con su peso para poder accedqer despues y llenarlos
		map(lambda aSet: bag.append((set(),costs[aSet])),range(cantidadDeConjuntos))


		#La fila indica el numero del universo, en cuantos conjuntos esta y en cuales
		elementoDelUniverso = 1
		while line != '': #habria que chequear que elementoDelUniverso sea menor a universo
			line = data.readline().strip()
			if line == '':
				break

			cantidadDeConjuntosEnElQueEstaElemento = int(line.split(" ")[0])

			#Agregar la fila (el elemento) a los conjuntos correspondientes
			cantidadConjuntosQueYaAgregue = 0
			while cantidadConjuntosQueYaAgregue < cantidadDeConjuntosEnElQueEstaElemento:
				line = data.readline().strip()
				conjuntosQueContienenElemento = line.split(" ")

				for conjunto in conjuntosQueContienenElemento:
					if conjunto == '\n':
						break

					#Los conjuntos van de 1..cantidadDeConjuntos pero mi lista va de 0..(cantidadDeConjuntos -1)
					indice = int(conjunto)-1 if int(conjunto) > 0 else int(conjunto)
					bag[indice][0].add(elementoDelUniverso)

					#Por cada conjunto, sumo 1
					cantidadConjuntosQueYaAgregue += 1					

			#Cuando termine con un numero (una fila) entonces sigo con el proximo, el while principal
			elementoDelUniverso += 1
			
		# print bag

	finally:
		data.close()


	return (bag,universo)

def test():
	bag = parse('parser-test.txt')
	packSet =  Pack.generatePackSetFromBag(bag)
	print packSet

def parseSCP2(fileName):
	return parse(PROJECT_ROOT+'/test-problems/scp/'+fileName[0:4]+'/'+fileName)

