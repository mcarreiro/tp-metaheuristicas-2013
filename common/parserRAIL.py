from structures import Pack

def parse(filename):
	data = open(filename, "r")

	bag = []
	
	tmp_set = None
	set_size = 0
	set_cost = 0

	try:		
		#Skipeo la primera
		line = data.readline().strip()
		
		while line != '':
			line = data.readline().strip()
			if line == '':
				break

			values = line.split(" ")
			tmp_set = set()
			set_cost = int(values.pop(0))
			set_size = int(values.pop(1))

			for value in values:
				if value == '\n':
					break
				tmp_set.add(int(value))

			bag.append((tmp_set, set_cost))	

	finally:
		data.close()


	return bag

def test():
	bag = parse("rail-test")
	print Pack.generatePackSetFromBag(bag)

def parseRAIL(fileName):
	return parse(PROJECT_ROOT+'/test-problems/rail/'+fileName)