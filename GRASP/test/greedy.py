'''
Created on Jul 26, 2013

@author: prago
'''
import sys
sys.path.append("../..")
from common.greedy import Greedy
from GRASP.localSearch import LocalSearch
from common.structures import *

instance = set()
idx=0

idx+=1
instance.add( Pack(idx, {1,2,3}, 6) )

idx+=1
instance.add( Pack(idx, {2,3}, 8) )

idx+=1
instance.add( Pack(idx, {2,3}, 8) )

idx+=1
instance.add( Pack(idx, {2,3,4}, 6) )

idx+=1
instance.add( Pack(idx, {4,5}, 16) )

idx+=1
instance.add( Pack(idx, {1,5}, 7) )

print instance

greedy = Greedy(5, instance, k=2)
  
solution = greedy.execute()
  
print solution

