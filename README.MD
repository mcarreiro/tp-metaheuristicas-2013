TP Metaheurísticas 2013
================================


Este es el TP de Metaheuristicas DC - UBA - 1° Cuatrimestre 2013.

Solución Colonia de Hormigas (Ant Colony) y GRASP al problema de Set K-Cover

Para poder utilizarlo, instalar Fabric 

	(En Linux: pip install fabric)

y correr como:

	fab pablo
	fab tincher 

Ejemplos de corrida en fabfile.py


Compilar .tex:
	
	colocar archivos .sty en ~/Documents/texmf/
	export TEXINPUTS=~/Documents/texmf///:${TEXINPUTS}
	pdflatex articulo.tex
