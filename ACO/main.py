"""
@idea: Esta parte deberia ser para ver la solucion y utilizar metodos en common.runner
@idea: Medir tiempos (en segundos)
@idea: Para jugar con parametros tenemos el fabfile
"""

from common.runner import packSetFromFile,cap
from aco import ACO
import time

def executeACO(fileName,k,amountAnts,alpha,iterations):
	(packSet,universe) = packSetFromFile(fileName)

	# packSet = cap(universe,packSet)

	start = time.clock()
	solution = ACO(packSet,int(amountAnts),int(universe),int(k),float(alpha),int(iterations),Q=1000000,initialPheromone=1,pheromones=None,evaporationCoefficient=0.4).execute()[0]
	end = time.clock()

	print 'Tiempo Total: ', end - start	
	print 'Solucion: ', solution.cost

	return (solution.cost, end - start)

def executeACOProfilingSolutions(fileName,alpha,amountAnts,iterations,k):
	(packSet,universe) = packSetFromFile(fileName)

	# packSet = cap(universe,packSet)

	start = time.clock()
	allSolutions = ACO(packSet,int(amountAnts),int(universe),int(k),float(alpha),int(iterations),Q=1000000,initialPheromone=0.1,pheromones=None,evaporationCoefficient=0.4).execute()[1]
	end = time.clock()

	return allSolutions
