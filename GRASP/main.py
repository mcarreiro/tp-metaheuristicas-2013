"""
@idea: Esta parte deberia ser para ver la solucion y utilizar metodos en common.runner
@idea: Medir tiempos (en segundos)
@idea: Para jugar con parametros tenemos el fabfile
"""

from common.runner import packSetFromFile
from grasp import Grasp
import time

     

def executeGrasp(fileName,iterations,k,factor):
    (allPacks,max) = packSetFromFile(fileName)
    
#     print str(max)
    
    solver = Grasp(int(max), allPacks, iterations=int(iterations), k=int(k), factor=float(factor))

    start = time.clock()
    solution = solver.execute()[0]
    end = time.clock()
    
    elapsed = end - start
    print 'Tiempo Total: ', elapsed
    
#     print ""
    print "solution.cost: %s"%(solution.cost)
#     print "\nFINISHED !!!"

    return (solution.cost,elapsed)



def executeGraspProfilingSolutions(fileName,iterations,k,factor):
    (allPacks,max) = packSetFromFile(fileName)
    
#     print str(max)
    
    solver = Grasp(int(max), allPacks, iterations=int(iterations), k=int(k), factor=float(factor))

    start = time.clock()
    (finalSolution, allSolutions) = solver.execute()
    end = time.clock()
    
    elapsed = end - start
    print 'Tiempo Total: ', elapsed
    
#     print ""
    print "solution.cost: %s"%(finalSolution.cost)
#     print "\nFINISHED !!!"

    return allSolutions

# executeGrasp("scpe1.txt",1,1,1)
