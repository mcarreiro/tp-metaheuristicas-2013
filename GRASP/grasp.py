'''
Created on Aug 3, 2013

@author: pablo
'''

from sys import maxint

from common.greedy import Greedy
from GRASP.localSearch import LocalSearch
from common.structures import Solution

from random import Random
import time

class Grasp:

    def __init__(self, universeCardinal, allPacks, iterations, k=1, factor=0, seed=None):
        self.universeCardinal = universeCardinal
        self.k = k
        self.factor = factor
        self.allPacks = allPacks
        self.iterations = iterations

        self.seed = Random().random()
        
        self.bestSolution = Solution(packs={}, cost=maxint)
        self.greedy = Greedy(self.universeCardinal, self.allPacks, self.k, self.factor, self.seed)
        self.localSearch = LocalSearch(self.universeCardinal, self.allPacks, self.k)

        
    
    
    def execute(self):
        
        solutionsAllAlong = []
        
        start_time = time.clock()
        
        for i in range(self.iterations):

            self.seed = Random().random()
#             print "starting greedy..."
            greedySolution = self.greedy.execute()
            # print "greedy returned a solution of cost %s and length %s" % (greedySolution.cost,len(greedySolution.packs))
            
#             print "starting local search..."
            localSearchSolution = self.localSearch.execute(greedySolution)
            # print "local search returned a solution of cost %s and length %s" % (greedySolution.cost,len(greedySolution.packs))
             
            if localSearchSolution.cost < self.bestSolution.cost:
                self.bestSolution = localSearchSolution
                
            timeSincetheBeggining = time.clock() - start_time
            solutionsAllAlong.append((self.bestSolution.cost, timeSincetheBeggining))
            
            self.greedy = Greedy(self.universeCardinal, self.allPacks, self.k, self.factor, self.seed)
            
        return (self.bestSolution, solutionsAllAlong)
            