'''
Created on Jul 24, 2013

@author: prago
'''
from __future__ import division
from random import Random
from collections import Counter

from common.structures import Pack
from common.structures import Solution

class Greedy:

    def __init__(self, universeCardinal, allPacks, k=1, factor=0, seed=None,sortKey=lambda aPack: aPack.effectiveCost):
        self.universeCardinal = universeCardinal
        self.k = k
        self.factor = factor
        self.solutionSet = set()
        self.universe = Counter()

        self.activePacks = []        
        self.untouchedPacksByID = dict()

        for aPack in allPacks:
            self.untouchedPacksByID[aPack.id] = aPack
            
            newPack = aPack.clone()
            self.activePacks.append(newPack)

        self.randGenerator = Random()
        if seed == None:
            seed = self.randGenerator.random()
        self.randGenerator.seed(seed)
        
        # print "seed: " + str(seed)
        
        self.sortKey = sortKey

    #FOR GRASP
    def execute(self):
        while not self.hasFinished():

            chosenPack = self.choosePack()
            self.basicPostChoose(chosenPack)

        return Solution(self.solutionSet)

    #FOR ACO
    def executeFromToken(self):
        while not self.hasFinished():

            chosenPack = self.choosePackFromToken()
            self.basicPostChoose(chosenPack)
            self.updatePacksToken()

        return Solution(self.solutionSet)



    def basicPostChoose(self,chosenPack):
        self.solutionSet.add( Pack(chosenPack.id, self.untouchedPacksByID[chosenPack.id].set, chosenPack.cost) )

        self.updateUniverse(chosenPack)

        self.updatePacks()

    
    
    def hasFinished(self):
        solutionFound = len(list(self.universe.elements())) >= (self.universeCardinal * self.k) 
        
        if (not solutionFound and len(self.activePacks) == 0):
            # print "Error: there is no possible k-covering"
            raise Exception("Error: there is no possible k-covering for k: ",self.k)
            
        return solutionFound
    

    def updateUniverse(self, chosenPack):
        for elem in chosenPack.set:
            self.universe[elem] += 1


    def updatePacks(self):
        
        for aPack in self.activePacks:
            aPack.set = filter(lambda elem: self.k - self.universe[elem] != 0, aPack.set)
        
        self.activePacks = filter(lambda aPack: len(aPack.set) != 0, self.activePacks)
        
        for aPack in self.activePacks:
            effectiveCardinalityWeighted = sum(map(lambda elem: self.k - self.universe[elem], aPack.set))
            aPack.effectiveCost = aPack.cost / effectiveCardinalityWeighted

    def updatePacksToken(self):
        #Tengo que hacerlo despues de update, si no el sum es falso
        total = sum(aPack.alpha*aPack.effectiveCost * aPack.beta*aPack.pheromone for aPack in self.activePacks)

        map(lambda aPack: aPack.setToken((aPack.alpha*aPack.effectiveCost * aPack.beta*aPack.pheromone) / total), self.activePacks)

    def choosePack(self):

        self.activePacks.sort(key = self.sortKey)

        cmin = self.activePacks[0].effectiveCost
        cmax = self.activePacks[len(self.activePacks)-1].effectiveCost
        threshold = cmin + self.factor * (cmax - cmin)

        return self.selectFromRCL(threshold)

    def choosePackFromToken(self):

        maxLotteryNumber = sum(aPack.token for aPack in self.activePacks)
        lotteryNumber = self.randGenerator.uniform(0,maxLotteryNumber)
        acum = 0

        self.activePacks.sort(key=lambda aPack: aPack.token,reverse=True) #Lo puse reverse asi realmente tinen mejores posibilidades
        # self.activePacks.sort(key=lambda aPack: aPack.token,reverse=False) #Lo puse reverse asi realmente tinen mejores posibilidades

        winningIdxPack = 0
        for pack in self.activePacks:
            acum += pack.token
            if acum > lotteryNumber:
                chosenPack = self.activePacks[winningIdxPack]                
                del self.activePacks[winningIdxPack]
                # print 'Pheromona de la elegida: ', chosenPack.pheromone
                return chosenPack
            winningIdxPack += 1


    def selectFromRCL(self, threshold):
        # Even if $factor (factor of randomness) is zero, if more than one different active pack happen to have the same effective cost
        # then the algorithm will select one at random from them. NOT the first occurrence           
        lastAcceptable = 0
        for idx, aPack in enumerate(self.activePacks):
            lastAcceptable = idx
            if aPack.effectiveCost > threshold:
                break

        chosenOne = self.randGenerator.randint(0,lastAcceptable)
        chosenPack = self.activePacks[chosenOne]
        del self.activePacks[chosenOne]

        return chosenPack

