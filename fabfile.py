"""
@idea: Tenemos este archivo para tener las pruebas reales que hacemos
@idea: Esto llama al execute
"""

from __future__ import division 
from fabric.api import env
import copy
from ACO.main import executeACO, executeACOProfilingSolutions
from GRASP.main import executeGrasp, executeGraspProfilingSolutions
import os,itertools

env.warn_only = True

def pablo(fileName,iterations,k,factor):
	return executeGrasp(fileName,iterations,k,factor)	
#PABLO ANOTA ACA SUS PRUEBAS
#fab pablo:scp42.txt,10,1,1


def tincher(fileName,k,amountAnts,alpha,iterations):
	return executeACO(fileName,k,amountAnts,alpha,iterations)	
#TINCHER ANOTA ACA ABAJO TODO LO QUE VOY PROBANDO Y QUEDA CLARO
#fab tincher:scp41.txt,1,10,1,1

def allFiles():
	#Esto lo tuve que hacer porque el os.walk si bien es recursivo, 
	#no se por que carajo repite los archivos
	#Entonces con SET respeto que no haya ni 1 repetido
	#Lo de Frozenset es por un tema de hasheo de Python con los items del set
	#Ya que los set son mutables, tengo que usar frozenset
	allFiles = set()
	for dirpath,dirnames,filenames in os.walk('test-problems/scp'):
		filesFrom1Type = set()
		for fileName in filenames:	
			filesFrom1Type.add(fileName)
		allFiles.add(frozenset(filesFrom1Type))

	return allFiles

def greatestKForEachFile():
	maxK = 100
	allFilesSet = allFiles()
	for oneTypeFileSet in allFilesSet:
		for fileName in oneTypeFileSet:
			greatestK = 0
			try:
				for k in range(maxK):
					k += 1 #Para no empezar del cero
					executeACO(fileName,k,1,1,1,1)
					greatestK = k
			except Exception as e:
				print 'Greatest K for file: ',fileName,' is: ',greatestK

MAX_K_PER_FILE = dict()

def readNuevoFile():
	crs = open("nuevo", "r")
	for raw in crs:
		MAX_K_PER_FILE[raw.split()[4]] = raw.split()[6]


#-------------------------------------------------------------------------------------------------------------------


# ACO_ARGS_KEYS=['amountAnts','alpha','iterations','k']
# GRASP_ARGS_KEYS=['factor','iterations','k']

def execWithParameterProduct(args,args_keys,heuristic,output_subdir,id_key_variable=0):
	#Lo que hace es hace el producto entre todas las sublistas
	#Y para cada combinacion, arma los argumentos correctos
	#Y lo ejecuta para TODOS los archivos
	#Abajo de todo hay ejemplos de como correrlo
	#Como que id_key_variable no cambie de 0 porque si no no anda :P
	fixedArgs = dict()
	slicedListWithOutTheVariableParameters = args[1:]
	allPossibleCombinations = list(itertools.product(*slicedListWithOutTheVariableParameters))
	allFilesSet = allFiles()	

	for combination in allPossibleCombinations:
		for idx,value in enumerate(combination):
			idx += 1 #porque el cero es el item a iterar que despues se va a pisar
			fixedArgs[args_keys[idx]] = value
		fixedArgs[args_keys[id_key_variable]] = args[id_key_variable]
		execAll(fixedArgs, heuristic, output_subdir,allFilesSet,args_keys[id_key_variable])

def execAll(fixedArgs, heuristic, output_subdir,allFilesSet,arg_key_variable):
	resultsAvg = []
	resultsFull = []
	
	for fixedValueOfIterable in fixedArgs[arg_key_variable]:
		fixedArgs[arg_key_variable] = fixedValueOfIterable
		
		for oneTypeFileSet in allFilesSet:
			solutionCosts = []
			elapsedTimes = []
			
			print 'oneTypeFileSet: ', str(oneTypeFileSet)
			for fileName in oneTypeFileSet:	
				try:
					print fileName, 'for ', fixedArgs
					if( fileAbleForK(fileName.split('.')[0], fixedArgs['k']) ):
						(solutionCost, elapsedTime) = heuristic(fileName, fixedArgs)
						solutionCosts.append(solutionCost)
						elapsedTimes.append(elapsedTime)
						resultsFull.append((fileName.split('.')[0], solutionCost, elapsedTime, fixedValueOfIterable))
					else:
						print '--------------------No possible k covering!!--------------------'
				except Exception as e:
					print e
			#Cuando termine con todos los archivos de un sub-scp ejemplo (scp4) hace el promedio
			if len(solutionCosts) != 0:
				solutionCostsAvg = avg(solutionCosts)
				elapsedTimesAvg = avg(elapsedTimes)
				resultsAvg.append((fileName[0:4], solutionCostsAvg, elapsedTimesAvg, fixedValueOfIterable))

	print "\nRESULTS\n"
	for res in resultsAvg:
		print res 
		
	writeToFile(resultsAvg, arg_key_variable, fixedArgs, output_subdir)
	
	writeToFileFull(resultsFull, arg_key_variable, fixedArgs, output_subdir)

def execGraspForArgs(fileName, args):
	#Aca se podria ver si el K lo elejimos en base el dict MAX_K_PER_FILE
	#args[k] = random o MAX_K_PER_FILE[fileName]
	return executeGrasp(fileName, args['iterations'], args['k'], args['factor'])

def execACOForArgs(fileName, args):
	#Aca se podria ver si el K lo elejimos en base el dict MAX_K_PER_FILE
	#args[k] = random(1,MAX_K_PER_FILE[fileName]) o MAX_K_PER_FILE[fileName]		
	return executeACO(fileName, args['k'], args['amountAnts'], args['alpha'], args['iterations'])

		
def avg(ls):
	return sum(ls) / len(ls)


def writeToFileFull(results, var_arg_key, args, output_subdir):
	writeToFile(results, var_arg_key, args, output_subdir+'/full')
	
	
def writeToFile(results, var_arg_key, args, output_subdir):
	import datetime
	name = 'var_'+var_arg_key+'__'
	for k,v in args.iteritems():
		if k != var_arg_key:
			name += str(k)+str(v)+'_'
	name = name[:len(name)-1]
	name += '__out__'
	name += str(datetime.datetime.utcnow())
	
	fullPath = 'output/'+output_subdir+'/'+name
	outputFile = open(fullPath, "w")
	
	try:
		for res in results:
			outputFile.write('%s\t%s\t%s\t%s\n'%res)
	finally:
		outputFile.close()

maxKByFile={ 'scp47': 1, 'scp41': 1, 'scpc2': 1, 'scp54': 1, 'scpc3': 1, 'scp53': 1, 'scp55': 1, 'scp51': 1, 'scpa4': 1, 'scp43': 1, 'scp59': 1, 'scp510': 1, 'scpa1': 1, 'scp42': 1, 'scp48': 1, 'scpa2': 1, 'scp49': 1, 'scpc5': 1, 'scp410': 1, 'scp58': 1, 'scpe3': 1, 'scp56': 1, 'scp46': 1, 'scpa5': 1, 'scp45': 1, 'scpc4': 1, 'scp44': 1, 'scp52': 1, 'scpc1': 1, 'scpa3': 1, 'scp57': 1, 'scp61': 2, 'scpe1': 2, 'scp64': 2, 'scp65': 2, 'scp62': 2, 'scpb4': 3, 'scp63': 3, 'scpe2': 3, 'scpe4': 3, 'scpb3': 4, 'scpb1': 4, 'scpb5': 4, 'scpe5': 4, 'scpb2': 5, 'scpd4': 7, 'scpd1': 7, 'scpd5': 7, 'scpd3': 7, 'scpd2': 8 }
def fileAbleForK(filename, k):
	return (maxKByFile[filename] >= k)


#Forma de Correrlo, que se cague FABRIC
# readNuevoFile()


def run(filename, iterations, factors):
	
	outstream = ""
	
	for it in iterations:
		outstream += str(it) + "\t"
		for factor in factors:
			args = {'factor': factor, 'iterations': it, 'k':1}
			print filename, 'for ', args
			(solutionCost, elapsedTime) = execGraspForArgs(filename, args)
			outstream += str(solutionCost) + "\t"
		outstream = outstream[0:len(outstream)-1]
		outstream += "\n"
		
	print outstream


# run('scp41.txt', [2,4,8,16,32,64,128], [0, 0.1, 0.2, 0.3])
# run('scp41.txt', [2,4,8,16], [0])


def runAll(filenames, iterations, factor):
	
	headers = "iterations"
	for filename in filenames:
		headers += "\t"+filename+"_cost"+"\t"+filename+"_time"
	
	print "headers: " + headers 
	
	outstream = ""	
	outstream += headers+"\n"
	for it in iterations:
		outstream += str(it) + "\t"
		
		for filename in filenames:
			args = {'factor': factor, 'iterations': it, 'k':1}
			print filename, 'for ', args
			(solutionCost, elapsedTime) = execGraspForArgs(filename, args)
			outstream += str(solutionCost) + "\t" + str(elapsedTime) + "\t"
		
		outstream = outstream[0:len(outstream)-1]
		outstream += "\n"
		
	print outstream

# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64,128,256], 0)
# runAll(['scpe1.txt', 'scpe2.txt', 'scpe3.txt', 'scpe4.txt'], [2,4,8,16,32,64,128,256], 0)

def runGraspProfiling(fileName, iterations, k, factor):
	solutionsEachIteration = executeGraspProfilingSolutions(fileName, iterations, k, factor)
	
	outstream = ""
	i = 0
	for (cost, time) in solutionsEachIteration:
		i+=1
		outstream += "%s\t%s\t%s\n"%(i,cost,time)
		
	print outstream
# runGraspProfiling("scp53.txt", 128, 24, 0)



def runACOProfiling(filename, alpha,amountAnts,iterations,k):
	solutionsEachIteration = executeACOProfilingSolutions(filename,alpha,amountAnts,iterations,k)
	
	outstream = ""
	i = 0
	for cost in solutionsEachIteration:
		i+=1
		outstream += "%s\t%s\n"%(i,cost)
		
	print outstream

def runAllACO(filenames, iterations, alpha, amountAnts):
	
	headers = "iterations"
	for filename in filenames:
		headers += "\t"+filename+"_cost"+"\t"+filename+"_time"
	
	print "headers: " + headers 
	
	outstream = ""	
	outstream += headers+"\n"
	for it in iterations:
		outstream += str(it) + "\t"
		
		for filename in filenames:
			args = {'alpha': alpha,'amountAnts': amountAnts,'iterations': it, 'k':1}
			print filename, 'for ', args
			(solutionCost, elapsedTime) = execACOForArgs(filename, args)
			outstream += str(solutionCost) + "\t" + str(elapsedTime) + "\t"
		
		outstream = outstream[0:len(outstream)-1]
		outstream += "\n"
		
	print outstream
	

# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64,128], 0.25, 10)
# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64,128], 0.25, 20)
# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64,128], 0.25, 50)
# FIJAR HORMIGAS


# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64], 0.25, 20)
# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64], 0.5, 20)
# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64], 0.75, 20)
# FIJAR ALPHA

# runAll(['scp41.txt', 'scp51.txt', 'scpb1.txt', 'scpd1.txt'], [2,4,8,16,32,64,128], 0.5, 10)

# runACOProfiling("scp51.txt",0.25,20,50,1)




GRASP_ARGS_KEYS=['factor','iterations','k']
# execWithParameterProduct(args=[[0,0.25,0.5,0.75,1],[2,4,8,16],[1,2,3,4,5,6,7]],args_keys=GRASP_ARGS_KEYS,heuristic=execGraspForArgs,output_subdir='grasp')
# execWithParameterProduct(args=[[0,0.25,0.5,0.75,1],[1],[7]],args_keys=GRASP_ARGS_KEYS,heuristic=execGraspForArgs,output_subdir='grasp')
# execWithParameterProduct(args=[[0.25],[1],[1]],args_keys=GRASP_ARGS_KEYS,heuristic=execGraspForArgs,output_subdir='grasp')

# Ahora el pivote es aplha
ACO_ARGS_KEYS=['alpha','amountAnts','iterations','k']
# execWithParameterProduct(args=[[0.25,0.5,0.75],[5,10,15,20],[2,4,8,16,32,64],[1,2,3,4,5,6,7]],args_keys=ACO_ARGS_KEYS,heuristic=execACOForArgs,output_subdir='ACO')
				
				
#fileName,k,amountAnts,alpha,iterations				
# fab tincher:scp41.txt,1,60,0.25,30
# fab tincher:scpd1.txt,1,20,0.25,30

#fileName,iterations,k,factor
#fab pablo:scp41.txt,1,1,0.5
#fab pablo:scpd1.txt,1,1,0.5