#La matriz principal dice por cad anumeor cuanto pesa
#Entonces el SET es la suma de los costos de cad auno de sys integrantes

from structures import Pack
import os.path,sys

PROJECT_ROOT = os.path.abspath(os.path.join(os.path.dirname( __file__ ), '..'))


def parse(filename):
	data = open(filename, "r")

	n,m = 0,0
	costs = []
	bag = []

	tmp_set = None
	set_size = 0
	set_cost = 0

	try:		

		line = data.readline().strip()
		nm = line.split(" ")
		n = int(nm[0])
		m = int(nm[1])

		while len(costs) < m:
			line = data.readline().strip()
			values = line.split(" ")
			
			for cost in values:
				if cost == '\n':
					break
				costs.append(int(cost))

		
		while line != '':
			line = data.readline().strip()
			if line == '':
				break

			values = line.split(" ")
			tmp_set = set()
			set_size = int(values[0])
			set_cost = 0

			while len(tmp_set) < set_size:
				line = data.readline().strip()
				values = line.split(" ")

				for value in values:
					if value == '\n':
						break
					tmp_set.add(int(value))
					set_cost += costs[int(value)-1]

			bag.append((tmp_set, set_cost))	

	finally:
		data.close()


	return (bag,m)

def test():
	bag = parse("parser-test.txt")
	packSet =  Pack.generatePackSetFromBag(bag)
	print packSet

def parseSCP(fileName):
	return parse(PROJECT_ROOT+'/test-problems/scp/'+fileName[0:4]+'/'+fileName)

