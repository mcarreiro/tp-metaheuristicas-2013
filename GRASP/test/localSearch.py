'''
Created on Aug 2, 2013

@author: pablo
'''
import sys
sys.path.append("../..")

from common.greedy import Greedy
from GRASP.localSearch import LocalSearch
from common.structures import *

# First test case
# instance = set()
# idx=0
# idx+=1
# instance.add( Pack(idx, {1,2,3}, 6) )
# idx+=1
# instance.add( Pack(idx, {2,3,4}, 6) )
     
# initialSol = Solution(instance.copy())
     
# idx+=1
# instance.add( Pack(idx, {1,2}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,4}, 3) )
     
# print instance
# print initialSol
       
# localSearch = LocalSearch( 4, instance)
# newSolution = localSearch.execute(initialSol)
      
# print newSolution
# print "\n"


# # Second test case
# instance = set()
# idx=0
# idx+=1
# instance.add( Pack(idx, {1,2,3}, 6) )
# idx+=1
# instance.add( Pack(idx, {2,3,4}, 6) )
# idx+=1
# instance.add( Pack(idx, {2,5}, 8) )
# idx+=1
# instance.add( Pack(idx, {2,6}, 8) )
     
# initialSol = Solution(instance.copy())
     
# idx+=1
# instance.add( Pack(idx, {1,2}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,4}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,5}, 16) )
# idx+=1
# instance.add( Pack(idx, {3,6}, 16) )
     
# print instance
# print initialSol
       
# localSearch = LocalSearch( 6, instance)
# newSolution = localSearch.execute(initialSol)
      
# print newSolution
# print "\n"


# # Third test case
# instance = set()
# idx=0
# idx+=1
# instance.add( Pack(idx, {1,2,3}, 6) )
# idx+=1
# instance.add( Pack(idx, {2,3,4}, 6) )
# idx+=1
# instance.add( Pack(idx, {4,2}, 4) )
# idx+=1
# instance.add( Pack(idx, {1,3}, 4) )
       
       
# initialSol = Solution(instance.copy())
       
# idx+=1
# instance.add( Pack(idx, {1,2}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,4}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,4}, 16) )
# idx+=1
# instance.add( Pack(idx, {3,2}, 16) )
       
# print instance
# print initialSol
         
# localSearch = LocalSearch( 6, instance, k=2)
# newSolution = localSearch.execute(initialSol)
        
# print newSolution
# print "\n"



# # Fourth test case
# instance = set()
# idx=0
# idx+=1
# instance.add( Pack(idx, {1,2,3}, 6) )
# idx+=1
# instance.add( Pack(idx, {2,3,4}, 6) )
# idx+=1
# instance.add( Pack(idx, {4,2}, 4) )
# idx+=1
# instance.add( Pack(idx, {1,3}, 4) )
# idx+=1
# instance.add( Pack(idx, {2,3}, 8) )
       
       
# initialSol = Solution(instance.copy())
       
# idx+=1
# instance.add( Pack(idx, {1,2}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,4}, 3) )
# idx+=1
# instance.add( Pack(idx, {3,4}, 16) )
# idx+=1
# instance.add( Pack(idx, {3,2}, 16) )
       
# print instance
# print initialSol
         
# localSearch = LocalSearch( 6, instance, k=2)
# newSolution = localSearch.execute(initialSol)
        
# print newSolution
# print "\n"



# Fifth test case
instance = set()
idx=0
idx+=1
instance.add( Pack(idx, {2,3}, 6) )
idx+=1
instance.add( Pack(idx, {1,4}, 6) )
idx+=1
instance.add( Pack(idx, {4,2}, 4) )
idx+=1
instance.add( Pack(idx, {1,3}, 4) )
idx+=1
instance.add( Pack(idx, {2,3}, 8) )
idx+=1
instance.add( Pack(idx, {2,3}, 8) )

       
initialSol = Solution(instance.copy())
       
idx+=1
instance.add( Pack(idx, {1,2}, 3) )
idx+=1
instance.add( Pack(idx, {3,4}, 3) )
idx+=1
instance.add( Pack(idx, {3,4}, 16) )
idx+=1
instance.add( Pack(idx, {3,2}, 16) )
       
print instance
print ''
print ''
print initialSol.packs
print ''
print ''
         
localSearch = LocalSearch( 6, instance, k=2)
newSolution = localSearch.execute(initialSol)
        
print newSolution.packs
print "\n"